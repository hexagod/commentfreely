/*! jquery-comments.js 1.4.0
*
* (c) 2017 Joona Tykkyläinen, Viima Solutions Oy
* jquery-comments may be freely distributed under the MIT license.
* For all details and documentation:
* http://viima.github.io/jquery-comments/
*/
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        module.exports = function (root, jQuery) {
            if (jQuery === undefined) {
                if (typeof window !== 'undefined') {
                    jQuery = require('jquery');
                }
                else {
                    jQuery = require('jquery')(root);
                }
            }
            factory(jQuery);
            return jQuery;
        };
    } else {
        factory(jQuery);
    }
}(function ($) {
    var Comments = {
        $el: null,
        commentsById: {
        },
        dataFetched: false,
        currentSortKey: '',
        options: {
        },
        events: {
            'click': 'closeDropdowns',
            'paste': 'preSavePastedAttachments',
            'keydown [contenteditable]': 'saveOnKeydown',
            'focus [contenteditable]': 'saveEditableContent',
            'keyup [contenteditable]': 'checkEditableContentForChange',
            'paste [contenteditable]': 'checkEditableContentForChange',
            'input [contenteditable]': 'checkEditableContentForChange',
            'blur [contenteditable]': 'checkEditableContentForChange',
            'click .navigation li[data-sort-key]': 'navigationElementClicked',
            'click .navigation li.title': 'toggleNavigationDropdown',
            'click .commenting-field.main .textarea': 'showMainCommentingField',
            'click .commenting-field.main .close': 'hideMainCommentingField',
            'click .commenting-field .textarea': 'increaseTextareaHeight',
            'change .commenting-field .textarea': 'increaseTextareaHeight textareaContentChanged',
            'click .commenting-field:not(.main) .close': 'removeCommentingField',
            'click .commenting-field .send.enabled': 'postComment',
            'click .commenting-field .update.enabled': 'putComment',
            'click .commenting-field .delete.enabled': 'deleteComment',
            'click .commenting-field .attachments .attachment .delete': 'preDeleteAttachment',
            'change .commenting-field .upload.enabled input[type="file"]': 'fileInputChanged',
            'click li.comment button.upvote': 'upvoteComment',
            'click li.comment button.delete.enabled': 'deleteComment',
            'click li.comment .hashtag': 'hashtagClicked',
            'click li.comment .ping': 'pingClicked',
            'click li.comment ul.child-comments .toggle-all': 'toggleReplies',
            'click li.comment button.reply': 'replyButtonClicked',
            'click li.comment button.edit': 'editButtonClicked',
            'click li.comment button.flag': 'flagButtonClicked',
            'dragenter': 'showDroppableOverlay',
            'dragenter .droppable-overlay': 'handleDragEnter',
            'dragleave .droppable-overlay': 'handleDragLeaveForOverlay',
            'dragenter .droppable-overlay .droppable': 'handleDragEnter',
            'dragleave .droppable-overlay .droppable': 'handleDragLeaveForDroppable',
            'dragover .droppable-overlay': 'handleDragOverForOverlay',
            'drop .droppable-overlay': 'handleDrop',
            'click .dropdown.autocomplete': 'stopPropagation',
            'mousedown .dropdown.autocomplete': 'stopPropagation',
            'touchstart .dropdown.autocomplete': 'stopPropagation',
        },
        getDefaultOptions: function () {
            return {
                profilePictureURL: '',
                currentUserIsAdmin: false,
                currentUserId: null,
                spinnerIconURL: '',
                upvoteIconURL: '',
                replyIconURL: '',
                uploadIconURL: '',
                attachmentIconURL: '',
                noCommentsIconURL: '',
                closeIconURL: '',
                textareaPlaceholderText: 'Add a comment',
                newestText: 'Newest',
                oldestText: 'Oldest',
                popularText: 'Popular',
                attachmentsText: 'Attachments',
                sendText: 'Send',
                replyText: 'Reply',
                editText: 'Edit',
                editedText: 'Edited',
                flagText: 'Flag',
                flaggedText: 'Flagged',
                youText: 'You',
                saveText: 'Save',
                deleteText: 'Delete',
                newText: 'New',
                viewAllRepliesText: 'View all __replyCount__ replies',
                hideRepliesText: 'Hide replies',
                noCommentsText: 'No comments',
                noAttachmentsText: 'No attachments',
                attachmentDropText: 'Drop files here',
                textFormatter: function (text) {
                    return text
                },
                enableReplying: true,
                enableEditing: true,
                enableUpvoting: true,
                enableDeleting: true,
                enableAttachments: false,
                enableHashtags: false,
                enablePinging: false,
                enableDeletingCommentWithReplies: false,
                enableNavigation: true,
                postCommentOnEnter: false,
                forceResponsive: false,
                readOnly: false,
                defaultNavigationSortKey: 'newest',
                highlightColor: '#2793e6',
                deleteButtonColor: '#C9302C',
                scrollContainer: this.$el,
                roundProfilePictures: false,
                textareaRows: 2,
                textareaRowsOnFocus: 2,
                textareaMaxRows: 5,
                maxRepliesVisible: 2,
                fieldMappings: {
                    id: 'id',
                    parent: 'parent',
                    created: 'created',
                    modified: 'modified',
                    content: 'content',
                    attachments: 'attachments',
                    pings: 'pings',
                    creator: 'creator',
                    fullname: 'fullname',
                    profilePictureURL: 'profile_picture_url',
                    isNew: 'is_new',
                    createdByAdmin: 'created_by_admin',
                    createdByCurrentUser: 'created_by_current_user',
                    upvoteCount: 'upvote_count',
                    userHasUpvoted: 'user_has_upvoted'
                },
                searchUsers: function (term, success, error) {
                    success([])
                },
                getComments: function (success, error) {
                    success([])
                },
                postComment: function (commentJSON, success, error) {
                    success(commentJSON)
                },
                putComment: function (commentJSON, success, error) {
                    success(commentJSON)
                },
                deleteComment: function (commentJSON, success, error) {
                    success()
                },
                flagComment: function () {
                },
                upvoteComment: function (commentJSON, success, error) {
                    success(commentJSON)
                },
                validateAttachments: function (attachments, callback) {
                    return callback(attachments)
                },
                hashtagClicked: function (hashtag) {
                },
                pingClicked: function (userId) {
                },
                refresh: function () {
                },
                refreshAction: function () {
                },
                timeFormatter: function (time) {
                    return new Date(time).toLocaleDateString()
                }
            }
        },
        init: function (options, el) {
            this.$el = $(el);
            this.$el.addClass('jquery-comments');
            this.undelegateEvents();
            this.delegateEvents();
            (function (a) {
                (jQuery.browser = jQuery.browser || {
                }).mobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))
            })(navigator.userAgent || navigator.vendor || window.opera);
            if ($.browser.mobile) this.$el.addClass('mobile');
            this.options = $.extend(true, {
            }, this.getDefaultOptions(), options);
            ;
            if (this.options.readOnly) this.$el.addClass('read-only');
            this.currentSortKey = this.options.defaultNavigationSortKey;
            this.createCssDeclarations();
            this.fetchDataAndRender();
        },
        delegateEvents: function () {
            this.bindEvents(false);
        },
        undelegateEvents: function () {
            this.bindEvents(true);
        },
        bindEvents: function (unbind) {
            var bindFunction = unbind ? 'off' : 'on';
            for (var key in this.events) {
                var eventName = key.split(' ')[0];
                var selector = key.split(' ').slice(1).join(' ');
                var methodNames = this.events[key].split(' ');
                for (var index in methodNames) {
                    if (methodNames.hasOwnProperty(index)) {
                        var method = this[methodNames[index]];
                        method = $.proxy(method, this);
                        if (selector == '') {
                            this.$el[bindFunction](eventName, method);
                        } else {
                            this.$el[bindFunction](eventName, selector, method);
                        }
                    }
                }
            }
        },
        fetchDataAndRender: function () {
            var self = this;
            this.commentsById = {
            };
            this.$el.empty();
            var mainCommentingField = this.createMainCommentingFieldElement();
            this.createHTML(mainCommentingField);
            this.options.getComments(function (commentsArray) {
                var refreshButton = $('<button/>', {
                    text: 'Refresh',
                    id: 'refresh-btn',
                    class: 'long-btn',
                    click: function () {
                        self.refreshFunction();
                    }
                });
                mainCommentingField.append('<br>');
                mainCommentingField.append(refreshButton);
                var commentModels = commentsArray.map(function (commentsJSON) {
                    return self.createCommentModel(commentsJSON)
                });
                self.sortComments(commentModels, 'oldest');
                $(commentModels).each(function (index, commentModel) {
                    self.addCommentToDataModel(commentModel);
                });
                $(commentModels).each(function (index, commentModel) {
                    self.addCommentToDataModel(commentModel);
                });
                self.dataFetched = true;
                self.render();
            });
        },
        fetchNext: function () {
            var self = this;
            var spinner = this.createSpinner();
            this.$el.find('ul#comment-list').append(spinner);
            var success = function (commentModels) {
                $(commentModels).each(function (index, commentModel) {
                    self.createComment(commentModel);
                });
                spinner.remove();
            }
            var error = function () {
                spinner.remove();
            }
            this.options.getComments(success, error);
        },
        createCommentModel: function (commentJSON) {
            var commentModel = this.applyInternalMappings(commentJSON);
            commentModel.childs = [
            ];
            commentModel.hasAttachments = function () {
                return commentModel.attachments.length > 0;
            }
            return commentModel;
        },
        addCommentToDataModel: function (commentModel) {
            if (!(commentModel.id in this.commentsById)) {
                this.commentsById[commentModel.id] = commentModel;
            } else {
                if (commentModel.parent) {
                    var outermostParent = this.getOutermostParent(commentModel.parent);
                    if (outermostParent != undefined) {
                        outermostParent.childs.push(commentModel.id);
                    }
                }
            }
        },
        updateCommentModel: function (commentModel) {
            $.extend(this.commentsById[commentModel.id], commentModel);
        },
        render: function () {
            var self = this;
            if (!this.dataFetched) return;
            this.showActiveContainer();
            this.createComments();
            if (this.options.enableAttachments) this.createAttachments();
            this.$el.find('> .spinner').remove();
            this.options.refresh();
        },
        refreshFunction: function () {
            this.options.refreshAction();
        },
        showActiveContainer: function () {
            var activeNavigationEl = this.$el.find('.navigation li[data-container-name].active');
            var containerName = activeNavigationEl.data('container-name');
            var containerEl = this.$el.find('[data-container="' + containerName + '"]');
            containerEl.siblings('[data-container]').hide();
            containerEl.show();
        },
        createComments: function () {
            var self = this;
            this.$el.find('#comment-list').remove();
            var commentList = $('<ul/>', {
                id: 'comment-list',
                'class': 'main'
            });
            var mainLevelComments = [
            ];
            var replies = [
            ];
            $(this.getComments()).each(function (index, commentModel) {
                if (commentModel.parent == null) {
                    mainLevelComments.push(commentModel);
                } else {
                    replies.push(commentModel);
                }
            });
            this.sortComments(mainLevelComments, this.currentSortKey);
            $(mainLevelComments).each(function (index, commentModel) {
                self.addComment(commentModel, commentList);
            });
            this.sortComments(replies, 'oldest');
            $(replies).each(function (index, commentModel) {
                self.addComment(commentModel, commentList);
            });
            this.$el.find('[data-container="comments"]').prepend(commentList);
        },
        createAttachments: function () {
            var self = this;
            this.$el.find('#attachment-list').remove();
            var attachmentList = $('<ul/>', {
                id: 'attachment-list',
                'class': 'main'
            });
            var attachments = this.getAttachments();
            this.sortComments(attachments, 'newest');
            $(attachments).each(function (index, commentModel) {
                self.addAttachment(commentModel, attachmentList);
            });
            this.$el.find('[data-container="attachments"]').prepend(attachmentList);
        },
        addComment: function (commentModel, commentList, prependComment) {
            commentList = commentList || this.$el.find('#comment-list');
            var commentEl = this.createCommentElement(commentModel);
            if (commentModel.parent) {
                var directParentEl = commentList.find('.comment[data-id="' + commentModel.parent + '"]');
                this.reRenderCommentActionBar(commentModel.parent);
                var outerMostParent = directParentEl.parents('.comment').last();
                if (outerMostParent.length == 0) outerMostParent = directParentEl;
                var childCommentsEl = outerMostParent.find('.child-comments');
                var commentingField = childCommentsEl.find('.commenting-field');
                if (commentingField.length) {
                    commentingField.before(commentEl)
                } else {
                    childCommentsEl.append(commentEl);
                }
                this.updateToggleAllButton(outerMostParent);
            } else {
                if (prependComment) {
                    commentList.prepend(commentEl);
                } else {
                    commentList.append(commentEl);
                }
            }
        },
        addAttachment: function (commentModel, commentList) {
            commentList = commentList || this.$el.find('#attachment-list');
            var commentEl = this.createCommentElement(commentModel);
            commentList.prepend(commentEl);
        },
        removeComment: function (commentId) {
            var self = this;
            var commentModel = this.commentsById[commentId];
            var childComments = this.getChildComments(commentModel.id);
            $(childComments).each(function (index, childComment) {
                self.removeComment(childComment.id);
            });
            if (commentModel.parent) {
                var outermostParent = this.getOutermostParent(commentModel.parent);
                var indexToRemove = outermostParent.childs.indexOf(commentModel.id);
                outermostParent.childs.splice(indexToRemove, 1);
            }
            delete this.commentsById[commentId];
            var commentElements = this.$el.find('li.comment[data-id="' + commentId + '"]');
            var parentEl = commentElements.parents('li.comment').last();
            commentElements.remove();
            this.updateToggleAllButton(parentEl);
        },
        preDeleteAttachment: function (ev) {
            var commentingField = $(ev.currentTarget).parents('.commenting-field').first()
            var attachmentEl = $(ev.currentTarget).parents('.attachment').first();
            attachmentEl.remove();
            this.toggleSaveButton(commentingField);
        },
        preSaveAttachments: function (files, commentingField) {
            var self = this;
            if (files.length) {
                if (!commentingField) commentingField = this.$el.find('.commenting-field.main');
                var uploadButton = commentingField.find('.control-row .upload');
                var isReply = !commentingField.hasClass('main');
                var attachmentsContainer = commentingField.find('.control-row .attachments');
                var attachments = $(files).map(function (index, file) {
                    return {
                        mime_type: file.type,
                        file: file
                    }
                });
                var existingAttachments = this.getAttachmentsFromCommentingField(commentingField);
                attachments = attachments.filter(function (index, attachment) {
                    var duplicate = false;
                    $(existingAttachments).each(function (index, existingAttachment) {
                        if (attachment.file.name == existingAttachment.file.name && attachment.file.size == existingAttachment.file.size) {
                            duplicate = true;
                        }
                    });
                    return !duplicate;
                });
                if (commentingField.hasClass('main')) {
                    commentingField.find('.textarea').trigger('click');
                }
                this.setButtonState(uploadButton, false, true);
                this.options.validateAttachments(attachments, function (validatedAttachments) {
                    if (validatedAttachments.length) {
                        $(validatedAttachments).each(function (index, attachment) {
                            var attachmentTag = self.createAttachmentTagElement(attachment, true);
                            attachmentsContainer.append(attachmentTag);
                        });
                        self.toggleSaveButton(commentingField);
                    }
                    self.setButtonState(uploadButton, true, false);
                });
            }
            uploadButton.find('input').val('');
        },
        updateToggleAllButton: function (parentEl) {
            if (this.options.maxRepliesVisible == null) return;
            var childCommentsEl = parentEl.find('.child-comments');
            var childComments = childCommentsEl.find('.comment').not('.hidden');
            var toggleAllButton = childCommentsEl.find('li.toggle-all');
            childComments.removeClass('togglable-reply');
            if (this.options.maxRepliesVisible === 0) {
                var togglableReplies = childComments;
            } else {
                var togglableReplies = childComments.slice(0, - this.options.maxRepliesVisible);
            }
            togglableReplies.addClass('togglable-reply');
            if (toggleAllButton.find('span.text').text() == this.options.textFormatter(this.options.hideRepliesText)) {
                togglableReplies.addClass('visible');
            }
            if (childComments.length > this.options.maxRepliesVisible) {
                if (!toggleAllButton.length) {
                    toggleAllButton = $('<li/>', {
                        'class': 'toggle-all highlight-font-bold'
                    });
                    var toggleAllButtonText = $('<span/>', {
                        'class': 'text'
                    });
                    var caret = $('<span/>', {
                        'class': 'caret'
                    });
                    toggleAllButton.append(toggleAllButtonText).append(caret);
                    childCommentsEl.prepend(toggleAllButton);
                }
                this.setToggleAllButtonText(toggleAllButton, false);
            } else {
                toggleAllButton.remove();
            }
        },
        updateToggleAllButtons: function () {
            var self = this;
            var commentList = this.$el.find('#comment-list');
            commentList.find('.comment').removeClass('visible');
            commentList.children('.comment').each(function (index, el) {
                self.updateToggleAllButton($(el));
            });
        },
        sortComments: function (comments, sortKey) {
            var self = this;
            if (sortKey == 'popularity') {
                comments.sort(function (commentA, commentB) {
                    var pointsOfA = commentA.childs.length;
                    var pointsOfB = commentB.childs.length;
                    if (self.options.enableUpvoting) {
                        pointsOfA += commentA.upvoteCount;
                        pointsOfB += commentB.upvoteCount;
                    }
                    if (pointsOfB != pointsOfA) {
                        return pointsOfB - pointsOfA;
                    } else {
                        var createdA = new Date(commentA.created).getTime();
                        var createdB = new Date(commentB.created).getTime();
                        return createdB - createdA;
                    }
                });
            } else {
                comments.sort(function (commentA, commentB) {
                    var createdA = new Date(commentA.created).getTime();
                    var createdB = new Date(commentB.created).getTime();
                    if (sortKey == 'oldest') {
                        return createdA - createdB;
                    } else {
                        return createdB - createdA;
                    }
                });
            }
        },
        sortAndReArrangeComments: function (sortKey) {
            var commentList = this.$el.find('#comment-list');
            var mainLevelComments = this.getComments().filter(function (commentModel) {
                return !commentModel.parent
            });
            this.sortComments(mainLevelComments, sortKey);
            $(mainLevelComments).each(function (index, commentModel) {
                var commentEl = commentList.find('> li.comment[data-id=' + commentModel.id + ']');
                commentList.append(commentEl);
            });
        },
        showActiveSort: function () {
            var activeElements = this.$el.find('.navigation li[data-sort-key="' + this.currentSortKey + '"]');
            this.$el.find('.navigation li').removeClass('active');
            activeElements.addClass('active');
            var titleEl = this.$el.find('.navigation .title');
            if (this.currentSortKey != 'attachments') {
                titleEl.addClass('active');
                titleEl.find('header').html(activeElements.first().html());
            } else {
                var defaultDropdownEl = this.$el.find('.navigation ul.dropdown').children().first();
                titleEl.find('header').html(defaultDropdownEl.html());
            }
            this.showActiveContainer();
        },
        forceResponsive: function () {
            this.$el.addClass('responsive');
        },
        closeDropdowns: function () {
            this.$el.find('.dropdown').hide();
        },
        preSavePastedAttachments: function (ev) {
            var clipboardData = ev.originalEvent.clipboardData;
            var files = clipboardData.files;
            if (files && files.length == 1) {
                var commentingField;
                var parentCommentingField = $(ev.target).parents('.commenting-field').first();
                if (parentCommentingField.length) {
                    commentingField = parentCommentingField;
                }
                this.preSaveAttachments(files, commentingField);
                ev.preventDefault();
            }
        },
        saveOnKeydown: function (ev) {
            if (ev.keyCode == 13) {
                var metaKey = ev.metaKey || ev.ctrlKey;
                if (this.options.postCommentOnEnter || metaKey) {
                    var el = $(ev.currentTarget);
                    el.siblings('.control-row').find('.save').trigger('click');
                    ev.stopPropagation();
                    ev.preventDefault();
                }
            }
        },
        saveEditableContent: function (ev) {
            var el = $(ev.currentTarget);
            el.data('before', el.html());
        },
        checkEditableContentForChange: function (ev) {
            var el = $(ev.currentTarget);
            $(el[0].childNodes).each(function () {
                if (this.nodeType == Node.TEXT_NODE && this.length == 0 && this.removeNode) this.removeNode();
            });
            if (el.data('before') != el.html()) {
                el.data('before', el.html());
                el.trigger('change');
            }
        },
        navigationElementClicked: function (ev) {
            var navigationEl = $(ev.currentTarget);
            var sortKey = navigationEl.data().sortKey;
            if (sortKey == 'attachments') {
                this.createAttachments();
            } else {
                this.sortAndReArrangeComments(sortKey);
            }
            this.currentSortKey = sortKey;
            this.showActiveSort();
        },
        toggleNavigationDropdown: function (ev) {
            ev.stopPropagation();
            var dropdown = $(ev.currentTarget).find('~ .dropdown');
            dropdown.toggle();
        },
        showMainCommentingField: function (ev) {
            var mainTextarea = $(ev.currentTarget);
            mainTextarea.siblings('.control-row').show();
            mainTextarea.parent().find('.close').show();
            mainTextarea.parent().find('.upload.inline-button').hide();
            mainTextarea.focus();
        },
        hideMainCommentingField: function (ev) {
            var closeButton = $(ev.currentTarget);
            var commentingField = this.$el.find('.commenting-field.main');
            var mainTextarea = commentingField.find('.textarea');
            var mainControlRow = commentingField.find('.control-row');
            this.clearTextarea(mainTextarea);
            commentingField.find('.attachments').empty();
            this.toggleSaveButton(commentingField);
            this.adjustTextareaHeight(mainTextarea, false);
            mainControlRow.hide();
            closeButton.hide();
            mainTextarea.parent().find('.upload.inline-button').show();
            mainTextarea.blur();
        },
        increaseTextareaHeight: function (ev) {
            var textarea = $(ev.currentTarget);
            this.adjustTextareaHeight(textarea, true);
        },
        textareaContentChanged: function (ev) {
            var textarea = $(ev.currentTarget);
            if (!textarea.find('.reply-to.tag').length) {
                var commentId = textarea.attr('data-comment');
                if (commentId) {
                    var parentComments = textarea.parents('li.comment');
                    if (parentComments.length > 1) {
                        var parentId = parentComments.last().data('id');
                        textarea.attr('data-parent', parentId);
                    }
                } else {
                    var parentId = textarea.parents('li.comment').last().data('id');
                    textarea.attr('data-parent', parentId);
                }
            }
            var commentingField = textarea.parents('.commenting-field').first();
            if (textarea[0].scrollHeight > textarea.outerHeight()) {
                commentingField.addClass('commenting-field-scrollable');
            } else {
                commentingField.removeClass('commenting-field-scrollable');
            }
            this.toggleSaveButton(commentingField);
        },
        toggleSaveButton: function (commentingField) {
            var textarea = commentingField.find('.textarea');
            var saveButton = textarea.siblings('.control-row').find('.save');
            var content = this.getTextareaContent(textarea, true);
            var attachments = this.getAttachmentsFromCommentingField(commentingField);
            var enabled;
            if (commentModel = this.commentsById[textarea.attr('data-comment')]) {
                var contentChanged = content != commentModel.content;
                var parentFromModel;
                if (commentModel.parent) {
                    parentFromModel = commentModel.parent.toString();
                }
                var parentChanged = textarea.attr('data-parent') != parentFromModel;
                var attachmentsChanged = false;
                if (this.options.enableAttachments) {
                    var savedAttachmentIds = commentModel.attachments.map(function (attachment) {
                        return attachment.id
                    });
                    var currentAttachmentIds = attachments.map(function (attachment) {
                        return attachment.id
                    });
                    attachmentsChanged = !this.areArraysEqual(savedAttachmentIds, currentAttachmentIds);
                }
                enabled = contentChanged || parentChanged || attachmentsChanged;
            } else {
                enabled = Boolean(content.length) || Boolean(attachments.length);
            }
            if (this.options.currentUserId != 'anonymous') {
                saveButton.toggleClass('enabled', enabled);
            }
        },
        removeCommentingField: function (ev) {
            var closeButton = $(ev.currentTarget);
            var textarea = closeButton.siblings('.textarea');
            if (textarea.attr('data-comment')) {
                closeButton.parents('li.comment').first().removeClass('edit');
            }
            var commentingField = closeButton.parents('.commenting-field').first();
            commentingField.remove();
        },
        postComment: function (ev) {
            var self = this;
            var sendButton = $(ev.currentTarget);
            var commentingField = sendButton.parents('.commenting-field').first();
            this.setButtonState(sendButton, false, true);
            var commentJSON = this.createCommentJSON(commentingField);
            commentJSON = this.applyExternalMappings(commentJSON);
            var success = function (commentJSON) {
                self.createComment(commentJSON);
                commentingField.find('.close').trigger('click');
                self.setButtonState(sendButton, false, false);
            };
            var error = function () {
                self.setButtonState(sendButton, true, false);
            };
            this.options.postComment(commentJSON, success, error);
        },
        createComment: function (commentJSON) {
            var commentModel = this.createCommentModel(commentJSON);
            this.addCommentToDataModel(commentModel);
            var commentList = this.$el.find('#comment-list');
            var prependComment = this.currentSortKey == 'newest';
            this.addComment(commentModel, commentList, prependComment);
            if (this.currentSortKey == 'attachments' && commentModel.hasAttachments()) {
                this.addAttachment(commentModel);
            }
        },
        putComment: function (ev) {
            var self = this;
            var saveButton = $(ev.currentTarget);
            var commentingField = saveButton.parents('.commenting-field').first();
            var textarea = commentingField.find('.textarea');
            this.setButtonState(saveButton, false, true);
            var commentJSON = $.extend({
            }, this.commentsById[textarea.attr('data-comment')]);
            $.extend(commentJSON, {
                parent: textarea.attr('data-parent') || null,
                content: this.getTextareaContent(textarea),
                pings: this.getPings(textarea),
                modified: new Date().getTime(),
                attachments: this.getAttachmentsFromCommentingField(commentingField)
            });
            commentJSON = this.applyExternalMappings(commentJSON);
            var success = function (commentJSON) {
                var commentModel = self.createCommentModel(commentJSON);
                delete commentModel['childs'];
                self.updateCommentModel(commentModel);
                commentingField.find('.close').trigger('click');
                self.reRenderComment(commentModel.id);
                self.setButtonState(saveButton, false, false);
            };
            var error = function () {
                self.setButtonState(saveButton, true, false);
            };
            this.options.putComment(commentJSON, success, error);
        },
        deleteComment: function (ev) {
            var self = this;
            var deleteButton = $(ev.currentTarget);
            var commentEl = deleteButton.parents('.comment').first();
            var commentJSON = $.extend({
            }, this.commentsById[commentEl.attr('data-id')]);
            var commentId = commentJSON.id;
            var parentId = commentJSON.parent;
            this.setButtonState(deleteButton, false, true);
            commentJSON = this.applyExternalMappings(commentJSON);
            var success = function () {
                self.removeComment(commentId);
                if (parentId) self.reRenderCommentActionBar(parentId);
                self.setButtonState(deleteButton, false, false);
            };
            var error = function () {
                self.setButtonState(deleteButton, true, false);
            };
            this.options.deleteComment(commentJSON, success, error);
        },
        hashtagClicked: function (ev) {
            var el = $(ev.currentTarget);
            var value = el.attr('data-value');
            this.options.hashtagClicked(value);
        },
        pingClicked: function (ev) {
            var el = $(ev.currentTarget);
            var value = el.attr('data-value');
            this.options.pingClicked(value);
        },
        fileInputChanged: function (ev, files) {
            var files = ev.currentTarget.files;
            var commentingField = $(ev.currentTarget).parents('.commenting-field').first();
            this.preSaveAttachments(files, commentingField);
        },
        upvoteComment: function (ev) {
            var self = this;
            var commentEl = $(ev.currentTarget).parents('li.comment').first();
            var commentModel = commentEl.data().model;
            var previousUpvoteCount = commentModel.upvoteCount;
            var newUpvoteCount;
            if (commentModel.userHasUpvoted) {
                newUpvoteCount = previousUpvoteCount - 1;
            } else {
                newUpvoteCount = previousUpvoteCount + 1;
            }
            commentModel.userHasUpvoted = !commentModel.userHasUpvoted;
            commentModel.upvoteCount = newUpvoteCount;
            this.reRenderUpvotes(commentModel.id);
            var commentJSON = $.extend({
            }, commentModel);
            commentJSON = this.applyExternalMappings(commentJSON);
            var success = function (commentJSON) {
                var commentModel = self.createCommentModel(commentJSON);
                self.updateCommentModel(commentModel);
                self.reRenderUpvotes(commentModel.id);
            };
            var error = function () {
                commentModel.userHasUpvoted = !commentModel.userHasUpvoted;
                commentModel.upvoteCount = previousUpvoteCount;
                self.reRenderUpvotes(commentModel.id);
            };
            this.options.upvoteComment(commentJSON, success, error);
        },
        toggleReplies: function (ev) {
            var el = $(ev.currentTarget);
            el.siblings('.togglable-reply').toggleClass('visible');
            this.setToggleAllButtonText(el, true);
        },
        replyButtonClicked: function (ev) {
            var replyButton = $(ev.currentTarget);
            var outermostParent = replyButton.parents('li.comment').last();
            var parentId = replyButton.parents('.comment').first().data().id;
            var replyField = outermostParent.find('.child-comments > .commenting-field');
            if (replyField.length) replyField.remove();
            var previousParentId = replyField.find('.textarea').attr('data-parent');
            if (previousParentId != parentId) {
                replyField = this.createCommentingFieldElement(parentId);
                outermostParent.find('.child-comments').append(replyField);
                var textarea = replyField.find('.textarea');
                this.moveCursorToEnd(textarea);
                this.ensureElementStaysVisible(replyField);
            }
        },
        editButtonClicked: function (ev) {
            var editButton = $(ev.currentTarget);
            var commentEl = editButton.parents('li.comment').first();
            var commentModel = commentEl.data().model;
            commentEl.addClass('edit');
            var editField = this.createCommentingFieldElement(commentModel.parent, commentModel.id);
            commentEl.find('.comment-wrapper').first().append(editField);
            var textarea = editField.find('.textarea');
            textarea.attr('data-comment', commentModel.id);
            textarea.append(this.getFormattedCommentContent(commentModel, true));
            this.moveCursorToEnd(textarea);
            this.ensureElementStaysVisible(editField);
        },
        flagButtonClicked: function (ev) {
            var flagButton = $(ev.currentTarget);
            var commentEl = flagButton.parents('li.comment').first();
            var commentModel = commentEl.data().model;
            var flagReason = prompt('Why are you flagging this comment?', '');
            if (flagReason != null) {
                data = {
                    flag_reason: flagReason,
                    id: commentEl.data().id
                }
                this.options.flagComment(data);
            }
        },
        showDroppableOverlay: function (ev) {
            if (this.options.enableAttachments) {
                this.$el.find('.droppable-overlay').css('top', this.$el[0].scrollTop);
                this.$el.find('.droppable-overlay').show();
                this.$el.addClass('drag-ongoing');
            }
        },
        handleDragEnter: function (ev) {
            var count = $(ev.currentTarget).data('dnd-count') || 0;
            count++;
            $(ev.currentTarget).data('dnd-count', count);
            $(ev.currentTarget).addClass('drag-over');
        },
        handleDragLeave: function (ev, callback) {
            var count = $(ev.currentTarget).data('dnd-count');
            count--;
            $(ev.currentTarget).data('dnd-count', count);
            if (count == 0) {
                $(ev.currentTarget).removeClass('drag-over');
                if (callback) callback();
            }
        },
        handleDragLeaveForOverlay: function (ev) {
            var self = this;
            this.handleDragLeave(ev, function () {
                self.hideDroppableOverlay();
            });
        },
        handleDragLeaveForDroppable: function (ev) {
            this.handleDragLeave(ev);
        },
        handleDragOverForOverlay: function (ev) {
            ev.stopPropagation();
            ev.preventDefault();
            ev.originalEvent.dataTransfer.dropEffect = 'copy';
        },
        hideDroppableOverlay: function () {
            this.$el.find('.droppable-overlay').hide();
            this.$el.removeClass('drag-ongoing');
        },
        handleDrop: function (ev) {
            ev.preventDefault();
            $(ev.target).trigger('dragleave');
            this.hideDroppableOverlay();
            this.preSaveAttachments(ev.originalEvent.dataTransfer.files);
        },
        stopPropagation: function (ev) {
            ev.stopPropagation();
        },
        createHTML: function (mainCommentingField) {
            var self = this;
            this.$el.append(mainCommentingField);
            var mainControlRow = mainCommentingField.find('.control-row');
            mainControlRow.hide();
            mainCommentingField.find('.close').hide();
            if (this.options.enableNavigation) {
                this.$el.append(this.createNavigationElement());
                this.showActiveSort();
            }
            var spinner = this.createSpinner();
            this.$el.append(spinner);
            var commentsContainer = $('<div/>', {
                'class': 'data-container',
                'data-container': 'comments'
            });
            this.$el.append(commentsContainer);
            var noComments = $('<div/>', {
                'class': 'no-comments no-data',
                text: this.options.textFormatter(this.options.noCommentsText)
            });
            var noCommentsIcon = $('<i/>', {
                'class': 'fa fa-comments fa-2x'
            });
            if (this.options.noCommentsIconURL.length) {
                noCommentsIcon.css('background-image', 'url("' + this.options.noCommentsIconURL + '")');
                noCommentsIcon.addClass('image');
            }
            noComments.prepend($('<br/>')).prepend(noCommentsIcon);
            commentsContainer.append(noComments);
        },
        createProfilePictureElement: function (src, userId) {
            if (src) {
                var profilePicture = $('<div/>').css({
                    'background-image': 'url(' + src + ')'
                });
            } else {
                var profilePicture = $('<i/>', {
                    'class': 'fa fa-user'
                });
            }
            profilePicture.addClass('profile-picture');
            profilePicture.attr('data-user-id', userId);
            if (this.options.roundProfilePictures) profilePicture.addClass('round');
            return profilePicture;
        },
        createMainCommentingFieldElement: function () {
            return this.createCommentingFieldElement(undefined, undefined, true);
        },
        createCommentingFieldElement: function (parentId, existingCommentId, isMain) {
            var self = this;
            var profilePictureURL;
            var userId;
            var attachments;
            var commentingField = $('<div/>', {
                'class': 'commenting-field'
            });
            if (isMain) commentingField.addClass('main');
            if (existingCommentId) {
                profilePictureURL = this.commentsById[existingCommentId].profilePictureURL;
                userId = this.commentsById[existingCommentId].creator;
                attachments = this.commentsById[existingCommentId].attachments;
            } else {
                profilePictureURL = this.options.profilePictureURL;
                userId = this.options.creator;
                attachments = [
                ];
            }
            var profilePicture = this.createProfilePictureElement(profilePictureURL, userId);
            var textareaWrapper = $('<div/>', {
                'class': 'textarea-wrapper'
            });
            var controlRow = $('<div/>', {
                'class': 'control-row'
            });
            var textarea = $('<div/>', {
                'class': 'textarea',
                'data-placeholder': this.options.textFormatter(this.options.textareaPlaceholderText),
                'onkeypress': 'return (this.innerText.length <= 5000)',
                contenteditable: true
            });
            this.adjustTextareaHeight(textarea, false);
            var closeButton = this.createCloseButton();
            closeButton.addClass('inline-button');
            var saveButtonClass = existingCommentId ? 'update' : 'send';
            var saveButtonText = existingCommentId ? this.options.textFormatter(this.options.saveText) : this.options.textFormatter(this.options.sendText);
            var saveButton = $('<span/>', {
                'class': saveButtonClass + ' save highlight-background',
                'text': saveButtonText
            });
            saveButton.data('original-content', saveButtonText);
            controlRow.append(saveButton);
            if (existingCommentId && this.isAllowedToDelete(existingCommentId)) {
                var deleteButtonText = this.options.textFormatter(this.options.deleteText);
                var deleteButton = $('<span/>', {
                    'class': 'delete enabled',
                    text: deleteButtonText
                }).css('background-color', this.options.deleteButtonColor);
                deleteButton.data('original-content', deleteButtonText);
                controlRow.append(deleteButton);
            }
            textareaWrapper.append(closeButton).append(textarea).append(controlRow);
            commentingField.append(profilePicture).append(textareaWrapper);
            if (parentId) {
                textarea.attr('data-parent', parentId);
                var parentModel = this.commentsById[parentId];
                if (parentModel.parent) {
                    textarea.html('&nbsp;');
                    var replyToName = '@' + parentModel.fullname;
                    var replyToTag = this.createTagElement(replyToName, 'reply-to', parentModel.creator, {
                        'data-user-id': parentModel.creator
                    });
                    textarea.prepend(replyToTag);
                }
            }
            if (this.options.enablePinging) {
                textarea.textcomplete([{
                    match: /(^|\s)@([^@]*)$/i,
                    index: 2,
                    search: function (term, callback) {
                        term = self.normalizeSpaces(term);
                        var error = function () {
                            callback([]);
                        }
                        self.options.searchUsers(term, callback, error);
                    },
                    template: function (user) {
                        var wrapper = $('<div/>');
                        var profilePictureEl = self.createProfilePictureElement(user.profile_picture_url);
                        var detailsEl = $('<div/>', {
                            'class': 'details',
                        });
                        var nameEl = $('<div/>', {
                            'class': 'name',
                        }).html(user.fullname);
                        var emailEl = $('<div/>', {
                            'class': 'email',
                        }).html(user.email);
                        if (user.email) {
                            detailsEl.append(nameEl).append(emailEl);
                        } else {
                            detailsEl.addClass('no-email')
                            detailsEl.append(nameEl)
                        }
                        wrapper.append(profilePictureEl).append(detailsEl);
                        return wrapper.html();
                    },
                    replace: function (user) {
                        var tag = self.createTagElement('@' + user.fullname, 'ping', user.id, {
                            'data-user-id': user.id
                        });
                        return ' ' + tag[0].outerHTML + ' ';
                    },
                }
                ], {
                        appendTo: '.jquery-comments',
                        dropdownClassName: 'dropdown autocomplete',
                        maxCount: 5,
                        rightEdgeOffset: 0,
                        debounce: 250
                    });
                $.fn.textcomplete.Dropdown.prototype.render = function (zippedData) {
                    var contentsHtml = this._buildContents(zippedData);
                    var unzippedData = $.map(zippedData, function (d) {
                        return d.value;
                    });
                    if (zippedData.length) {
                        var strategy = zippedData[0].strategy;
                        if (strategy.id) {
                            this.$el.attr('data-strategy', strategy.id);
                        } else {
                            this.$el.removeAttr('data-strategy');
                        }
                        this._renderHeader(unzippedData);
                        this._renderFooter(unzippedData);
                        if (contentsHtml) {
                            this._renderContents(contentsHtml);
                            this._fitToBottom();
                            this._fitToRight();
                            this._activateIndexedItem();
                        }
                        this._setScroll();
                    } else if (this.noResultsMessage) {
                        this._renderNoResultsMessage(unzippedData);
                    } else if (this.shown) {
                        this.deactivate();
                    }
                    var top = parseInt(this.$el.css('top')) + self.options.scrollContainer.scrollTop();
                    this.$el.css('top', top);
                    var originalLeft = this.$el.css('left');
                    this.$el.css('left', 0);
                    var maxLeft = self.$el.width() - this.$el.outerWidth();
                    var left = Math.min(maxLeft, parseInt(originalLeft));
                    this.$el.css('left', left);
                }
                $.fn.textcomplete.ContentEditable.prototype._skipSearch = function (clickEvent) {
                    switch (clickEvent.keyCode) {
                        case 9:
                        case 13:
                        case 16:
                        case 17:
                        case 33:
                        case 34:
                        case 40:
                        case 38:
                        case 27:
                            return true;
                    }
                    if (clickEvent.ctrlKey) switch (clickEvent.keyCode) {
                        case 78:
                        case 80:
                            return true;
                    }
                }
            }
            return commentingField;
        },
        createNavigationElement: function () {
            var navigationEl = $('<ul/>', {
                'class': 'navigation'
            });
            var navigationWrapper = $('<div/>', {
                'class': 'navigation-wrapper'
            });
            navigationEl.append(navigationWrapper);
            var newest = $('<li/>', {
                text: this.options.textFormatter(this.options.newestText),
                'data-sort-key': 'newest',
                'data-container-name': 'comments'
            });
            var oldest = $('<li/>', {
                text: this.options.textFormatter(this.options.oldestText),
                'data-sort-key': 'oldest',
                'data-container-name': 'comments'
            });
            var popular = $('<li/>', {
                text: this.options.textFormatter(this.options.popularText),
                'data-sort-key': 'popularity',
                'data-container-name': 'comments'
            });
            var dropdownNavigationWrapper = $('<div/>', {
                'class': 'navigation-wrapper responsive'
            });
            var dropdownNavigation = $('<ul/>', {
                'class': 'dropdown'
            });
            var dropdownTitle = $('<li/>', {
                'class': 'title'
            });
            var dropdownTitleHeader = $('<header/>');
            dropdownTitle.append(dropdownTitleHeader);
            dropdownNavigationWrapper.append(dropdownTitle);
            dropdownNavigationWrapper.append(dropdownNavigation);
            navigationEl.append(dropdownNavigationWrapper);
            navigationWrapper.append(newest).append(oldest);
            dropdownNavigation.append(newest.clone()).append(oldest.clone());
            if (this.options.enableReplying || this.options.enableUpvoting) {
                navigationWrapper.append(popular);
                dropdownNavigation.append(popular.clone());
            }
            if (this.options.forceResponsive) this.forceResponsive();
            return navigationEl;
        },
        createSpinner: function (inline) {
            var spinner = $('<div/>', {
                'class': 'spinner'
            });
            if (inline) spinner.addClass('inline');
            var spinnerIcon = $('<i/>', {
                'class': 'fa fa-spinner fa-spin'
            });
            if (this.options.spinnerIconURL.length) {
                spinnerIcon.css('background-image', 'url("' + this.options.spinnerIconURL + '")');
                spinnerIcon.addClass('image');
            }
            spinner.html(spinnerIcon);
            return spinner;
        },
        createCloseButton: function (className) {
            var closeButton = $('<span/>', {
                'class': className || 'close'
            });
            var icon = $('<i/>', {
                'class': 'fa fa-times'
            });
            if (this.options.closeIconURL.length) {
                icon.css('background-image', 'url("' + this.options.closeIconURL + '")');
                icon.addClass('image');
            }
            closeButton.html(icon);
            return closeButton;
        },
        createCommentElement: function (commentModel) {
            var commentEl = $('<li/>', {
                'data-id': commentModel.id,
                'class': 'comment'
            }).data('model', commentModel);
            if (commentModel.createdByCurrentUser) commentEl.addClass('by-current-user');
            if (commentModel.createdByAdmin) commentEl.addClass('by-admin');
            var childComments = $('<ul/>', {
                'class': 'child-comments'
            });
            var commentWrapper = this.createCommentWrapperElement(commentModel);
            commentEl.append(commentWrapper);
            if (commentModel.parent == null) commentEl.append(childComments);
            return commentEl;
        },
        createCommentWrapperElement: function (commentModel) {
            var self = this;
            var commentWrapper = $('<div/>', {
                'class': 'comment-wrapper'
            });
            var profilePicture = this.createProfilePictureElement(commentModel.profilePictureURL, commentModel.creator);
            var time = $('<time/>', {
                text: this.options.timeFormatter(commentModel.created),
                'data-original': commentModel.created
            });
            var commentHeaderEl = $('<div/>', {
                'class': 'comment-header',
            });
            var nameEl = $('<span/>', {
                'class': 'name',
                'data-user-id': commentModel.creator,
                'text': commentModel.createdByCurrentUser ? this.options.textFormatter(this.options.youText) : commentModel.fullname
            });
            commentHeaderEl.append(nameEl);
            if (commentModel.createdByAdmin) nameEl.addClass('highlight-font-bold');
            if (commentModel.parent) {
                var parent = this.commentsById[commentModel.parent];
                if (parent != undefined) {
                    if (parent.parent) {
                        var replyTo = $('<span/>', {
                            'class': 'reply-to',
                            'text': parent.fullname,
                            'data-user-id': parent.creator
                        });
                        var replyIcon = $('<i/>', {
                            'class': 'fa fa-share'
                        });
                        if (this.options.replyIconURL.length) {
                            replyIcon.css('background-image', 'url("' + this.options.replyIconURL + '")');
                            replyIcon.addClass('image');
                        }
                        replyTo.prepend(replyIcon);
                        commentHeaderEl.append(replyTo);
                    }
                }
            }
            if (commentModel.isNew) {
                var newTag = $('<span/>', {
                    'class': 'new highlight-background',
                    text: this.options.textFormatter(this.options.newText)
                });
                commentHeaderEl.append(newTag);
            }
            var wrapper = $('<div/>', {
                'class': 'wrapper'
            });
            var content = $('<div/>', {
                'class': 'content'
            });
            content.html(this.getFormattedCommentContent(commentModel));
            if (commentModel.modified && commentModel.modified != commentModel.created) {
                var editedTime = this.options.timeFormatter(commentModel.modified);
                var edited = $('<time/>', {
                    'class': 'edited',
                    text: this.options.textFormatter(this.options.editedText) + ' ' + editedTime,
                    'data-original': commentModel.modified
                });
                content.append(edited);
            }
            var attachments = $('<div/>', {
                'class': 'attachments'
            });
            var attachmentPreviews = $('<div/>', {
                'class': 'previews'
            });
            var attachmentTags = $('<div/>', {
                'class': 'tags'
            });
            attachments.append(attachmentPreviews).append(attachmentTags);
            if (this.options.enableAttachments && commentModel.hasAttachments()) {
                $(commentModel.attachments).each(function (index, attachment) {
                    var format = undefined;
                    var type = undefined;
                    if (attachment.mime_type) {
                        var mimeTypeParts = attachment.mime_type.split('/');
                        if (mimeTypeParts.length == 2) {
                            format = mimeTypeParts[1];
                            type = mimeTypeParts[0];
                        }
                    }
                    if (type == 'image' || type == 'video') {
                        var previewRow = $('<div/>');
                        var preview = $('<a/>', {
                            'class': 'preview',
                            href: attachment.file,
                            target: '_blank'
                        });
                        previewRow.html(preview);
                        if (type == 'image') {
                            var image = $('<img/>', {
                                src: attachment.file
                            });
                            preview.html(image);
                        } else {
                            var video = $('<video/>', {
                                src: attachment.file,
                                type: attachment.mime_type,
                                controls: 'controls'
                            });
                            preview.html(video);
                        }
                        attachmentPreviews.append(previewRow);
                    }
                });
            }
            var actions = $('<span/>', {
                'class': 'actions'
            });
            var separator = $('<span/>', {
                'class': 'separator',
                text: '·'
            });
            var reply = $('<button/>', {
                'class': 'action reply',
                'type': 'button',
                text: this.options.textFormatter(this.options.replyText)
            });
            var upvoteIcon = $('<i/>', {
                'class': 'fa fa-thumbs-up'
            });
            if (this.options.upvoteIconURL.length) {
                upvoteIcon.css('background-image', 'url("' + this.options.upvoteIconURL + '")');
                upvoteIcon.addClass('image');
            }
            var upvotes = this.createUpvoteElement(commentModel);
            if (this.options.enableReplying) actions.append(reply);
            if (this.options.enableUpvoting) actions.append(upvotes);
            if (commentModel.createdByCurrentUser || this.options.currentUserIsAdmin) {
                var editButton = $('<button/>', {
                    'class': 'action edit',
                    text: this.options.textFormatter(this.options.editText)
                });
                actions.append(editButton);
            }
            var flagButton = $('<button/>', {
                'class': 'action flag',
                text: this.options.textFormatter(this.options.flagText)
            });
            if (this.options.currentUserId != 'anonymous') {
                actions.append(flagButton);
            }
            actions.children().each(function (index, actionEl) {
                if (!$(actionEl).is(':last-child')) {
                    $(actionEl).after(separator.clone());
                }
            });
            wrapper.append(content);
            wrapper.append(attachments);
            wrapper.append(actions);
            commentWrapper.append(profilePicture).append(time).append(commentHeaderEl).append(wrapper);
            return commentWrapper;
        },
        createUpvoteElement: function (commentModel) {
            var upvoteIcon = $('<i/>', {
                'class': 'fa fa-thumbs-up'
            });
            if (this.options.upvoteIconURL.length) {
                upvoteIcon.css('background-image', 'url("' + this.options.upvoteIconURL + '")');
                upvoteIcon.addClass('image');
            }
            var upvoteEl = $('<button/>', {
                'class': 'action upvote' + (commentModel.userHasUpvoted ? ' highlight-font' : '')
            }).append(upvoteIcon).append($('<span/>', {
                text: commentModel.upvoteCount,
                'class': 'upvote-count'
            }))
            return upvoteEl;
        },
        createTagElement: function (text, extraClasses, value, extraAttributes) {
            var tagEl = $('<input/>', {
                'class': 'tag',
                'type': 'button',
                'data-role': 'none',
            });
            if (extraClasses) tagEl.addClass(extraClasses);
            tagEl.val(text);
            tagEl.attr('data-value', value);
            if (extraAttributes) tagEl.attr(extraAttributes);
            return tagEl;
        },
        reRenderComment: function (id) {
            var commentModel = this.commentsById[id];
            var commentElements = this.$el.find('li.comment[data-id="' + commentModel.id + '"]');
            var self = this;
            commentElements.each(function (index, commentEl) {
                var commentWrapper = self.createCommentWrapperElement(commentModel);
                $(commentEl).find('.comment-wrapper').first().replaceWith(commentWrapper);
            });
        },
        reRenderCommentActionBar: function (id) {
            var commentModel = this.commentsById[id];
            if (commentModel != undefined) {
                var commentElements = this.$el.find('li.comment[data-id="' + commentModel.id + '"]');
                var self = this;
                commentElements.each(function (index, commentEl) {
                    var commentWrapper = self.createCommentWrapperElement(commentModel);
                    $(commentEl).find('.actions').first().replaceWith(commentWrapper.find('.actions'));
                });
            }
        },
        reRenderUpvotes: function (id) {
            var commentModel = this.commentsById[id];
            var commentElements = this.$el.find('li.comment[data-id="' + commentModel.id + '"]');
            var self = this;
            commentElements.each(function (index, commentEl) {
                var upvotes = self.createUpvoteElement(commentModel);
                $(commentEl).find('.upvote').first().replaceWith(upvotes);
            });
        },
        createCssDeclarations: function () {
            $('head style.jquery-comments-css').remove();
            this.createCss('.jquery-comments ul.navigation li.active:after {background: '
                + this.options.highlightColor + ' !important;', + '}');
            this.createCss('.jquery-comments ul.navigation ul.dropdown li.active {background: '
                + this.options.highlightColor + ' !important;', + '}');
            this.createCss('.jquery-comments .highlight-background {background: '
                + this.options.highlightColor + ' !important;', + '}');
            this.createCss('.jquery-comments .highlight-font {color: '
                + this.options.highlightColor + ' !important;'
                + '}');
            this.createCss('.jquery-comments .highlight-font-bold {color: '
                + this.options.highlightColor + ' !important;'
                + 'font-weight: bold;'
                + '}');
        },
        createCss: function (css) {
            var styleEl = $('<style/>', {
                type: 'text/css',
                'class': 'jquery-comments-css',
                text: css
            });
            $('head').append(styleEl);
        },
        getComments: function () {
            var self = this;
            return Object.keys(this.commentsById).map(function (id) {
                return self.commentsById[id]
            });
        },
        getChildComments: function (parentId) {
            return this.getComments().filter(function (comment) {
                return comment.parent == parentId
            });
        },
        getAttachments: function () {
            return this.getComments().filter(function (comment) {
                return comment.hasAttachments()
            });
        },
        getOutermostParent: function (directParentId) {
            var parentId = directParentId;
            if (this.commentsById[parentId] != undefined) {
                do {
                    var parentComment = this.commentsById[parentId];
                    parentId = parentComment.parent;
                } while (parentComment.parent != null);
            }
            return parentComment;
        },
        createCommentJSON: function (commentingField) {
            var textarea = commentingField.find('.textarea');
            var time = new Date().toISOString();
            var commentJSON = {
                id: 'c' + (this.getComments().length + 1),
                parent: textarea.attr('data-parent') || null,
                created: time,
                modified: time,
                content: this.getTextareaContent(textarea),
                pings: this.getPings(textarea),
                fullname: this.options.textFormatter(this.options.youText),
                profilePictureURL: this.options.profilePictureURL,
                createdByCurrentUser: true,
                upvoteCount: 0,
                userHasUpvoted: false,
                attachments: this.getAttachmentsFromCommentingField(commentingField)
            };
            return commentJSON;
        },
        isAllowedToDelete: function (commentId) {
            if (this.options.enableDeleting) {
                var isAllowedToDelete = true;
                if (!this.options.enableDeletingCommentWithReplies) {
                    $(this.getComments()).each(function (index, comment) {
                        if (comment.parent == commentId) isAllowedToDelete = false;
                    });
                }
                return isAllowedToDelete;
            }
            return false;
        },
        setToggleAllButtonText: function (toggleAllButton, toggle) {
            var self = this;
            var textContainer = toggleAllButton.find('span.text');
            var caret = toggleAllButton.find('.caret');
            var showExpandingText = function () {
                var text = self.options.textFormatter(self.options.viewAllRepliesText);
                var replyCount = toggleAllButton.siblings('.comment').not('.hidden').length;
                text = text.replace('__replyCount__', replyCount);
                textContainer.text(text);
            };
            var hideRepliesText = this.options.textFormatter(this.options.hideRepliesText);
            if (toggle) {
                if (textContainer.text() == hideRepliesText) {
                    showExpandingText();
                } else {
                    textContainer.text(hideRepliesText);
                }
                caret.toggleClass('up');
            } else {
                if (textContainer.text() != hideRepliesText) {
                    showExpandingText();
                }
            }
        },
        setButtonState: function (button, enabled, loading) {
            button.toggleClass('enabled', enabled);
            if (loading) {
                button.html(this.createSpinner(true));
            } else {
                button.html(button.data('original-content'));
            }
        },
        adjustTextareaHeight: function (textarea, focus) {
            var textareaBaseHeight = 2.2;
            var lineHeight = 1.45;
            var setRows = function (rows) {
                var height = textareaBaseHeight + (rows - 1) * lineHeight;
                textarea.css('height', height + 'em');
            };
            textarea = $(textarea);
            var rowCount = focus == true ? this.options.textareaRowsOnFocus : this.options.textareaRows;
            do {
                setRows(rowCount);
                rowCount++;
                var isAreaScrollable = textarea[0].scrollHeight > textarea.outerHeight();
                var maxRowsUsed = this.options.textareaMaxRows == false ? false : rowCount > this.options.textareaMaxRows;
            } while (isAreaScrollable && !maxRowsUsed);
        },
        clearTextarea: function (textarea) {
            textarea.empty().trigger('input');
        },
        getTextareaContent: function (textarea, humanReadable) {
            var textareaClone = textarea.clone();
            textareaClone.find('.reply-to.tag').remove();
            textareaClone.find('.tag.hashtag').replaceWith(function () {
                return humanReadable ? $(this).val() : '#' + $(this).attr('data-value');
            });
            textareaClone.find('.tag.ping').replaceWith(function () {
                return humanReadable ? $(this).val() : '@' + $(this).attr('data-value');
            });
            var ce = $('<pre/>').html(textareaClone.html());
            ce.find('div, p, br').replaceWith(function () {
                return '\n' + this.innerHTML;
            });
            var text = ce.text().replace(/^\s+/g, '');
            var text = this.normalizeSpaces(text);
            return text;
        },
        getFormattedCommentContent: function (commentModel, replaceNewLines) {
            var html = this.escape(commentModel.content);
            html = this.linkify(html);
            html = this.highlightTags(commentModel, html);
            if (html.length > 400) {
                var splitPoint = html.indexOf(' ', 300) + 1;
                var partOne = html.substring(0, splitPoint);
                var partTwo = html.substring(splitPoint);
                html = '<p>' + partOne + '<span class="truncated">' + partTwo + '</span></p>'
            } else {
                html = '<p>' + html + '</p>';
            }
            if (replaceNewLines) html = html.replace(/(?:\n)/g, '<br>');
            return html;
        },
        getPings: function (textarea) {
            var pings = {
            };
            textarea.find('.ping').each(function (index, el) {
                var id = parseInt($(el).attr('data-value'));
                var value = $(el).val();
                pings[id] = value.slice(1);
            });
            return pings;
        },
        getAttachmentsFromCommentingField: function (commentingField) {
            var attachments = commentingField.find('.attachments .attachment').map(function () {
                return $(this).data();
            }).toArray();
            return attachments;
        },
        moveCursorToEnd: function (el) {
            el = $(el)[0];
            $(el).trigger('input');
            $(el).scrollTop(el.scrollHeight);
            if (typeof window.getSelection != 'undefined' && typeof document.createRange != 'undefined') {
                var range = document.createRange();
                range.selectNodeContents(el);
                range.collapse(false);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            } else if (typeof document.body.createTextRange != 'undefined') {
                var textRange = document.body.createTextRange();
                textRange.moveToElementText(el);
                textRange.collapse(false);
                textRange.select();
            }
            el.focus();
        },
        ensureElementStaysVisible: function (el) {
            var maxScrollTop = el.position().top;
            var minScrollTop = el.position().top + el.outerHeight() - this.options.scrollContainer.outerHeight();
            if (this.options.scrollContainer.scrollTop() > maxScrollTop) {
                this.options.scrollContainer.scrollTop(maxScrollTop);
            } else if (this.options.scrollContainer.scrollTop() < minScrollTop) {
                this.options.scrollContainer.scrollTop(minScrollTop);
            }
        },
        escape: function (inputText) {
            return $('<pre/>').text(this.normalizeSpaces(inputText)).html();
        },
        normalizeSpaces: function (inputText) {
            return inputText.replace(new RegExp(' ', 'g'), ' ');
        },
        after: function (times, func) {
            var self = this;
            return function () {
                times--;
                if (times == 0) {
                    return func.apply(self, arguments);
                }
            }
        },
        highlightTags: function (commentModel, html) {
            if (this.options.enableHashtags) html = this.highlightHashtags(commentModel, html);
            if (this.options.enablePinging) html = this.highlightPings(commentModel, html);
            return html;
        },
        highlightHashtags: function (commentModel, html) {
            var self = this;
            if (html.indexOf('#') != - 1) {
                var __createTag = function (tag) {
                    var tag = self.createTagElement('#' + tag, 'hashtag', tag);
                    return tag[0].outerHTML;
                }
                var regex = /(^|\s)#([a-zäöüß\d-_]+)/gim;
                html = html.replace(regex, function ($0, $1, $2) {
                    return $1 + __createTag($2);
                });
            }
            return html;
        },
        highlightPings: function (commentModel, html) {
            var self = this;
            if (html.indexOf('@') != - 1) {
                var __createTag = function (pingText, userId) {
                    var tag = self.createTagElement(pingText, 'ping', userId, {
                        'data-user-id': userId
                    });
                    return tag[0].outerHTML;
                }
                $(Object.keys(commentModel.pings)).each(function (index, userId) {
                    var fullname = commentModel.pings[userId];
                    var pingText = '@' + fullname;
                    html = html.replace(new RegExp(pingText, 'g'), __createTag(pingText, userId));
                });
            }
            return html;
        },
        linkify: function (inputText) {
            var replacedText,
                replacePattern1,
                replacePattern2,
                replacePattern3;
            replacePattern1 = /(\b(https?|ftp|file):\/\/[-A-ZÄÖÅ0-9+&@#\/%?=~_|!:,.;]*[-A-ZÄÖÅ0-9+&@#\/%=~_|])/gim;
            replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');
            replacePattern2 = /(^|[^\/f])(www\.[-A-ZÄÖÅ0-9+&@#\/%?=~_|!:,.;]*[-A-ZÄÖÅ0-9+&@#\/%=~_|])/gim;
            replacedText = replacedText.replace(replacePattern2, '$1<a href="https://$2" target="_blank">$2</a>');
            replacePattern3 = /(([A-ZÄÖÅ0-9\-\_\.])+@[A-ZÄÖÅ\_]+?(\.[A-ZÄÖÅ]{2,6})+)/gim;
            replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1" target="_blank">$1</a>');
            var count = inputText.match(/<a href/g) || [
            ];
            if (count.length > 0) {
                var splitInput = inputText.split(/(<\/a>)/g);
                for (var i = 0; i < splitInput.length; i++) {
                    if (splitInput[i].match(/<a href/g) == null) {
                        splitInput[i] = splitInput[i].replace(replacePattern1, '<a href="$1" target="_blank">$1</a>').replace(replacePattern2, '$1<a href="https://$2" target="_blank">$2</a>').replace(replacePattern3, '<a href="mailto:$1" target="_blank">$1</a>');
                    }
                }
                var combinedReplacedText = splitInput.join('');
                return combinedReplacedText;
            } else {
                return replacedText;
            }
        },
        waitUntil: function (condition, callback) {
            var self = this;
            if (condition()) {
                callback();
            } else {
                setTimeout(function () {
                    self.waitUntil(condition, callback);
                }, 100);
            }
        },
        areArraysEqual: function (array1, array2) {
            if (array1.length != array2.length) {
                return false;
            } else {
                array1.sort();
                array2.sort();
                for (var i = 0; i < array1.length; i++) {
                    if (array1[i] != array2[i]) return false;
                }
                return true;
            }
        },
        applyInternalMappings: function (commentJSON) {
            var invertedMappings = {
            };
            var mappings = this.options.fieldMappings;
            for (var prop in mappings) {
                if (mappings.hasOwnProperty(prop)) {
                    invertedMappings[mappings[prop]] = prop;
                }
            }
            return this.applyMappings(invertedMappings, commentJSON);
        },
        applyExternalMappings: function (commentJSON) {
            var mappings = this.options.fieldMappings;
            return this.applyMappings(mappings, commentJSON);
        },
        applyMappings: function (mappings, commentJSON) {
            var result = {
            };
            for (var key1 in commentJSON) {
                if (key1 in mappings) {
                    var key2 = mappings[key1];
                    result[key2] = commentJSON[key1];
                }
            }
            return result;
        }
    };
    $.fn.comments = function (options) {
        return this.each(function () {
            var comments = Object.create(Comments);
            $.data(this, 'comments', comments);
            comments.init(options || {
            }, this);
        });
    };
}));
