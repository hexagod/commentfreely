function initComments(cf_url, cf_auth, currentUserId, profilePictureURL, commentCount, refreshAction, isThreadAdmin) {
	window.commentCount=commentCount;
		$('#comments-container').comments({
			enableAttachments: true,
			readOnly: false,
			currentUserId: currentUserId,
			profilePictureURL: profilePictureURL,
			enableDeletingCommentWithReplies: true,
			currentUserIsAdmin: isThreadAdmin,
			textareaMaxRows:10,
			getComments: function(success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/get_comments/', 
					data: { cf_auth: cf_auth, commentCount: window.commentCount }, 
					success: function(userArray) { success(userArray); window.commentCount = userArray.length; }, 
					error: error 
				});
			},
			postComment: function(data, success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/create_comment/',
					data: { commentData: data, cf_auth: cf_auth },
					success: function(userArray) { success(userArray); window.commentCount++; },
					error: error
				});
			},
			putComment: function(data, success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/update_comment/',
					data: { commentData: data, cf_auth: cf_auth },
					error: error
				});
				success(data);
			},
			deleteComment: function(data, success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/delete_comment/',
					data: { commentData: data, cf_auth: cf_auth },
					success: function(userArray) { window.commentCount--; },
					error: error
				});
				success();
			},
			upvoteComment: function(data, success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/upvote_comment/',
					data: { commentData: data, cf_auth: cf_auth },
					error: error
				});
				success(data);
			},
			refresh: function() {
				$('.truncated').hide().after('<a title="expand text"> [show more]</a>').next().on('click', function () { 
					$(this).text() == ' [show less]' 
					? $(this).text(' [show more]').attr("title", "expand text")
					: $(this).text(' [show less]').attr("title", "collapse text");
					$(this).prev().toggle();
				});
			},
			flagComment: function(data) {
                $.ajax({ 
                    type: 'post', 
                    url: cf_url+'/api/flag_comment/',
                    data: { commentData: data, cf_auth: cf_auth },
                });
			},
			timeFormatter: function(time) {
    			return moment(time).fromNow();
  			},
  			refreshAction,
		});
}

