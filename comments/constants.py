class Constants():

    # Constants used to specify comment states.
    STATE_DELETED = 0
    STATE_SOFTDELETED = 50
    STATE_TRASH = 100
    STATE_SPAM = 200
    STATE_PENDING = 300
    STATE_VISIBLE = 400
    STATE_PLACEHOLDER = 500

    STATE_ILLEGAL = 1200
    STATE_RULES = 1300
    STATE_INVALID = 1400

    CONTENT_STATES = (
        (STATE_DELETED, 'Deleted and awaiting purge batch job.'),
        (STATE_TRASH, 'In the trash bin.'),
        (STATE_SPAM, 'SPAM.'),
        (STATE_PENDING, 'Awaiting page owners moderation.'),
        (STATE_VISIBLE, 'Visible to all.'),
        (STATE_PLACEHOLDER, 'Content removed.'),
    )

    FLAG_STATES = (
        (STATE_PENDING, 'Awaiting Moderation'),
        (STATE_SPAM, 'SPAM'),
        (STATE_ILLEGAL, 'Illegal'),
        (STATE_RULES, 'Rules violation'),
        (STATE_INVALID, 'Not a valid flagging'),
    )